package com.ITMO;

class DiningPhilosophers {

    static final Object forksLock = new Object();

    boolean[] forks = {true,true,true,true,true};

    public DiningPhilosophers() {

    }

    public void wantsToEat(int philosopher,
                           Runnable pickLeftFork,
                           Runnable pickRightFork,
                           Runnable eat,
                           Runnable putLeftFork,
                           Runnable putRightFork) throws InterruptedException {
        while(true){

            boolean LeftFork;
            boolean RightFork;
            synchronized (forksLock){
                LeftFork = forks[philosopher];
                RightFork = forks[(philosopher == 0) ? 4 : philosopher - 1];
            }

            if(LeftFork && RightFork){
                synchronized (forksLock){
                    forks[philosopher] = false;
                    forks[(philosopher == 0) ? 4 : philosopher - 1] = false;
                }

                pickLeftFork.run();
                pickRightFork.run();
                eat.run();
                putLeftFork.run();
                putRightFork.run();

                synchronized (forksLock){
                    forks[philosopher] = true;
                    forks[(philosopher == 0) ? 4 : philosopher - 1] = true;
                }
                break;
            }
            else Thread.sleep(5);
        }
    }
}